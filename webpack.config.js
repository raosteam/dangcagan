var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var BUILD_DIR = path.resolve(__dirname, 'src/build');
var APP_DIR = path.resolve(__dirname, 'src/app');
var CSS_DIR = path.resolve(__dirname, 'src/css');
var SRV_DIR = path.resolve(__dirname, 'src/server');

var configCommand = {
module: {
    loaders: [
        {
            test: /\.jsx?/,
            include: APP_DIR,
            exclude: path.resolve(__dirname, '/node_modules'),
            loader: 'babel-loader'
        },
        {
            test: /\.css$/,
            include: CSS_DIR,
            loader: ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: "css-loader"
            })
        }]
    },
plugins: [
        new ExtractTextPlugin({
        filename: 'style.css',
        allChunks: true
    })
    ]
}


var frontEnd = {
    entry: APP_DIR + '/index.jsx',
    output: {
        path: BUILD_DIR,
        filename: 'bundle.js'
    }
 };

var nodeBuild = {
    entry: SRV_DIR + '/main.js',
    output: {
        path: BUILD_DIR,
        filename: 'server.js'
    },
    target: 'node'
 };

module.exports = [
    Object.assign({} , configCommand, frontEnd),
    Object.assign({} , configCommand, nodeBuild)
];