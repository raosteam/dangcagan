import path from 'path';  
import express from 'express';  

const app = express();  
const port = 3000;
app.use(express.static(path.join(__dirname, '../app')));
app.use(express.static(path.join(__dirname, '../build')));

app.get('*', function response(req, res) {  
  res.sendFile('index.html');
});

var nodePort = 3000

console.log("path :" + __dirname);

var listener = app.listen(nodePort);  

console.log("Server listening at port: " + listener.address().port);