import React from 'react'
import {render} from 'react-dom'
import AboutComponent from './component/about/aboutPage.jsx';
import Header from './component/Common/header.jsx';

class App extends React.Component {
    render() {
        var Child;
        switch(this.props.route) {
            case 'about':
                Child = AboutComponent;
                break;
            default :
                Child = Header;
                break;
        }
        
        return (
        <div>
           <Child></Child>
        </div>
        );
    }
}

function processHash()
{
    var command = window.location.hash.substr(1);
    render(<App route={command}/>, document.getElementById('app'));
}

window.addEventListener('hashchange', processHash);
processHash();