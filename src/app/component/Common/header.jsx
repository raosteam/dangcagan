import React from 'react';

class Header extends React.Component {
    render() {
        return (
            <div>
                <nav>
                    <div>
                        <ul>
                            <li>
                                <a href="#about">About</a>
                            </li>
                            <li>
                                <a href="/">Home</a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <h1>Welcome to Dungcagan Municipality</h1>
            </div>
        );
    }
}

export default Header;